#!/usr/bin/env python3

from distutils.core import setup

setup( name='aqueduct'
     , packages=['aqueduct']
     , scripts=['aqueduct-worker']
     , version='0.0.1'

      # metadata for upload to PyPI
     , author='Pavel Shramov'
     , author_email='shramov@mexmat.net'
     , description='Redis queue processing'
     , long_description="""Redis queue processing"""
     , license='MIT'
     , keywords='redis queue'
     , url='https://bitbucket.org/gladiolus/aqueduct'
     , install_requires=[ "redis", "pyyaml" ]
     , classifiers=[ 'Development Status :: 2 - Alpha'
                   , 'Environment :: Console'
                   , 'Intended Audience :: Developers'
		   , 'License :: OSI Approved :: MIT License'
                   , 'Operating System :: OS Independent'
                   ]
     , platforms='All'
     )
