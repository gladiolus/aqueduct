#!/usr/bin/env python3
# vim: sts=4 sw=4 et

import json

class Slice:
    def __init__(self, id, d):
        self.id = id

        x = [x for x in d if x['source'] == 'url.netloc']
        if len(x) != 1:
            raise RuntimeError("Need exactly one netloc definition")

        self.netloc = x[0]['value']

        p = self.netloc.split('.')
        if p[0] == '':
            p = p[1:]
        self.netloc_path = list(reversed(p))

        self.domain = self.netloc.startswith('.')

        self.query = {}
        for x in [x for x in d if x['source'] == 'url.query_param']:
            self.query[x['key']] = x['value']

        # Already handled by self.netloc and self.query
        self.desc = [x for x in d if x['source'] not in ('url.netloc', 'url.query_param')]

    def match(self, x):
        ud = x['url']
        url = ud['netloc']
        #print("Check {} {}".format(self.netloc, url))
        if not self.domain:
            if self.netloc != url: return False
        else:
            if not url.endswith(self.netloc) and url != self.netloc[1:]:
                return False
        for k, v0 in self.query.items():
            v1 = ud['query_params'].get(k)
            if v1 != v0: return False
        for q in self.desc:
            if not self.match_one(x, **q):
                return False
        return True

    def match_one(self, x, source=None, condition='equal', value=None, key=None):
        d = x
        for p in source.split('.'):
            d = d.get(p)
            if d is None: return False
        v = d if key is None else d.get(key)
        if v is None: return False
        if condition == 'equal':
            if v != value: return False
        elif condition == 'starts':
            if not v.startswith(value): return False
        #print("Match one {} {} {}".format(source, value, x['url']))
        return True

class Node:
    def __init__(self, name):
        self.name = name
        self.sub = {}
        self.slices = []

def load(fp):
    root = Node('')
    for l in fp:
        i, l = l.split(' ', 1)
        d = json.loads(l)
        s = Slice(i, d)
        n = root
        for p in s.netloc_path:
            if p not in n.sub:
                n.sub[p] = Node(p)
            n = n.sub[p]
        n.slices.append(s)
        print(s.desc)
    return root

root = load(open('slices.json'))
def slices(pipe, x):
    url = x['url']['netloc'].split('.')
    n = root
    for p in reversed(url):
        n = n.sub.get(p)
        if n is None:
            break
        for s in n.slices:
            if s.match(x):
                pipe.incr('slice:' + s.id)

class FakePipe:
    def incr(self, id):
        print("Increment", id)

#slices(FakePipe(), {'url':{'netloc':'www.mts.ru'}})
