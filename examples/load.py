#!/usr/bin/env python3
# vim: sts=4 sw=4 et

import yaml, redis, time, lzma

r = redis.Redis()
r.delete('queue')
#pipe = r.pipeline(transaction=False)

#for l in open('mts.small.json', 'r'):
t, keys, blob = time.time(), [], []
for l in lzma.open('mts2M.data.xz', 'rt'):
    k, v = l.split(' ', 1)
    #v = yaml.load(v)['json:data']
    v = eval(v)['json:data'].encode('latin1').decode('utf-8')
    k = 'counter:' + k
    keys.append(k)
    blob.append((k, v))
    if len(keys) == 1000:
        pipe = r.pipeline()
        for k, v in blob:
            pipe.set(k, v)
        pipe.lpush('queue', *keys)
        pipe.execute()
        dt = time.time() - t
        print("Total: {:.3f}ms ({:.3f}m/ms); Data: {}".format(dt * 1000., len(keys) / (1000. * dt), len(keys)))
        t, keys, blob = time.time(), [], []
    #print(k, v)

#pipe.execute()
