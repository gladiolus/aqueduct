#!/usr/bin/env python
# vim: sts=4 sw=4 et

import happybase

class terminate:
    def __init__(self):
        self._conn = happybase.Connection('localhost')
        self._table = self._conn.table('hits')
        self._batch = None

    def __call__(self, r, x):
        if not self._batch:
            self._batch = self._table.batch()
        self._batch.put(x['uuid'], {'hit:uuid':x['trace_uuid'], 'hit:host':x['url']['netloc']})

    def flush(self, r):
        if not self._batch:
            return
        self._batch.send()
        self._batch = None
