#!/usr/bin/env python3
# vim: sts=4 sw=4 et

from aqueduct import with_flush, with_fetch

def hits_flush(p):
    pass

def hits_fetch(k):
    pass

@with_fetch(hits_fetch)
@with_flush(hits_flush)
def hits(p, x):
    p.incr('hits:' + x['url']['netloc'])

def hits_path(p, x):
    p.incr('hits:' + x['url']['netloc'] + x['url']['path'])
