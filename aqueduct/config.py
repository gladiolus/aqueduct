#!/usr/bin/env python
# vim: sts=4 sw=4 et

import yaml

def _load(url):
    if '://' not in url:
        return open(url)
    proto, path = url.split('://', 1)
    if proto == 'file':
        return open(path)
    elif proto in ('http', 'https', 'ftp'):
        import urllib.request
        return urllib.request.urlopen(url)
    elif proto == 'redis':
        import redis
        host, key = path.split('/', 1)
        rkw = {}
        if ':' in host:
            host, port = host.rsplit(':', 1)
            rkw['port'] = int(port)
        if host:
            rkw['host'] = host
        r = redis.Redis(**rkw)
        return r.get(key)

def config(url):
    return yaml.load(_load(url))
