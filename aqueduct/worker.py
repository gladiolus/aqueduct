#!/usr/bin/env python3
# vim: sts=4 sw=4 et

import json, redis, time
import logging
from .rcache import RedisCache

def with_flush(flush):
    def _f(f):
        f.flush = flush
        return f
    return _f

def with_fetch(fetch):
    def _f(f):
        f.fetch = fetch
        return f
    return _f

class Worker:
    def __init__(self, queue, name, processed=None, conn=None, batch=50, batch_min=None):
        self.log = logging.getLogger('aqueduct.worker.' + name)
        self._r = conn or redis.Redis()
        self._name = name
        self._iqueue = queue
        self._wqueue = queue + '-' + name
        self._pqueue = processed
        self._batch_max = batch
        self._batch_min = max(batch_min or 0, 10)
        self._batch = self._batch_min
        if self._batch_min >= self._batch_max:
            raise ValueError("Invalid batch size: min %d >= max %d" % (self._batch_max,self._batch_max))
        self._tasks = []
        self._tasks_prepare = []
        self._tasks_fetch = []
        self._tasks_flush = []
        self.total = 0
        self.log.info("Initialized with queue %s", self._iqueue)

    def register(self, task):
        self._tasks.append(task)
        f = getattr(task, 'flush', None)
        if callable(f):
            self._tasks_flush.append(f)
        f = getattr(task, 'fetch', None)
        if callable(f):
            self._tasks_fetch.append(f)
        f = getattr(task, 'prepare', None)
        if callable(f):
            self._tasks_prepare.append(f)

    def recover(self):
        l = self._r.llen(self._wqueue)
        if l == 0: return
        self.log.info("Recover %s entries", l)
        elem = self._r.lrange(self._wqueue, 0, l)
        self._r.rpush(self._iqueue, *elem)
        self._r.delete(self._wqueue)

    def process(self):
        t = time.time()
        p = self._r.pipeline(transaction=False)
        p.brpoplpush(self._iqueue, self._wqueue, 1)
        for i in range(self._batch - 1):
            p.rpoplpush(self._iqueue, self._wqueue)

        keys = p.execute()
        keys = [x for x in keys if x is not None]

        if len(keys) >= 0.75 * self._batch:
            self._batch = min(self._batch_max, self._batch * 2)
        elif len(keys) < self._batch / 2:
            self._batch = max(self._batch_min, int(self._batch * 0.75))

        if not keys:
            return 0
        extra = set()
        for k in keys:
            for t in self._tasks_fetch:
                extra.update(t(k) or [])
        extra = extra - set(keys)
        extra = list(extra)

        t = time.time()
        d = self._r.mget(keys + extra)
        p = RedisCache(self._r.pipeline(transaction=False))
        for k, x in zip(keys + extra, d):
            p.cache(k, x)
        for t in self._tasks_prepare:
            t(p)
        bad = []
        for k, x in zip(keys, d):
            try:
                #c = json.loads(x.decode('utf-8'))
                for t in self._tasks:
                    t(p, k)
            except Exception:
                self.log.exception("Failed to process entry %s:\n%s", k, x)
                bad.append(k)
        for t in self._tasks_flush:
            t(p)
        p.ltrim(self._wqueue, len(keys), -1)
        if bad:
            p.lpush('carantine:' + self._wqueue, reversed(bad))
        if self._pqueue:
            bad = set(bad)
            p.lpush(self._pqueue, *[x for x in reversed(keys) if x not in bad])
        p.incrby('stat:{}:processed'.format(self._name), len(keys))
        if bad: p.incrby('stat:{}:error'.format(self._name), len(bad))
        p.execute()
        self.total += len(keys)
        return len(keys)
