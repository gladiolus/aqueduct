#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: sts=4 sw=4 et

class RedisCache(object):
    def __init__(self, pipe):
        self._cache = {}
        self._set = set()
        self._pipe = pipe

    def cache(self, k, v):
        self._cache[k] = v

    def incr(self, *a): self._pipe.incr(*a)
    def incrby(self, *a): self._pipe.incrby(*a)
    def lpush(self, *a): self._pipe.lpush(*a)
    def rpush(self, *a): self._pipe.rpush(*a)
    def ltrim(self, *a): self._pipe.ltrim(*a)

    def get(self, key):
        return self._cache.get(key)

    def mget(self, *keys):
        return [self._cache.get(k) for k in keys]

    def set(self, key, value):
        self._cache[key] = value
        self._set.add(key)

    def mset(self, *a):
        if len(a) % 2 != 0:
            raise ValueError("MSET argument list must be odd")
        for i in range(0, len(a), 2):
            self.set(a[i], a[i+1])

    def delete(self, *a):
        """
        Удалить запись, отмечаются None
        """
        for k in a:
            self._cache[k] = None
        self._set.update(k)

    def execute(self):
        """
        Внести изменения. Если запись была удалена и перезаписана пройдет только set
        """
        s = []
        d = []
        for k in self._set:
            v = self._cache.get(k)
            if v is not None:
                r += [k, v]
            else:
                d += v
        if s:
            self._pipe.mset(*s)
        if d:
            self._pipe.delete(*d)
        return self._pipe.execute()
