#!/usr/bin/env python3
# vim: sts=4 sw=4 et

import redis, json

class Queue:
    def __init__(self, name, redis=None, connection_pool=None):
        self._redis = redis
        self._pool = connection_pool
        if not redis and not connection_pool:
            raise RuntimeError("Need either redis or connection_pool parameter")
        self._name = name

    def redis(self):
        if self._redis:
            return self._redis
        return redis.Redis(connection_pool = self._pool)

    def push(self, v):
        r = self.redis()
        r.lpush(self._name, v)
